<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css" />
	<title>
	Surya and Ryan's Filesharing Site
	</title>
</head>
<body>
	<h1>
		Welcome to our filesharing site!
	</h1>
	<form method="get">
		<p>
			Login or Create a New User:
			<input type="text" name="username"> <br>
			Existing User:
			<input type="radio" name="user_type" value="existing" checked> <br>
			New User:
			<input type="radio" name="user_type" value="new"> <br>

			<input type="submit" value="Submit">
	</form>

	<?php
		if(isset($_GET['username']) and isset($_GET['user_type'])){

			$user_type = $_GET['user_type'];
			$username = $_GET['username'];
			$users_file = fopen("/home/surya/hidden_files/users.txt", "r+");

			if ($user_type == "existing"){
				$verified = false;
				echo "<ul>\n";
				while( !feof($users_file) ){
					if ($username == trim(fgets($users_file))){
						$verified = true;
					}
				}
				echo "</ul>\n";
				fclose($users_file);
				if ($verified == true){
					session_start();
					$_SESSION['username'] = $username;
					header("Location: main.php");
				}
				else{
					header("Location: loginfail.html");
				}
			}

			else {
				echo "<ul>\n";
				while( !feof($users_file) ){
					if ($username == trim(fgets($users_file))){
						printf("<p> FAILURE: USERNAME HAS ALREADY BEEN ASSIGNED! </p>");
						exit;
					}
				}
				// username hasn't been taken, so we add it to the users.txt file and make a directory
				session_start();
				fwrite($users_file, sprintf("%s\n", $username));
				mkdir(sprintf("/srv/uploads/%s", $username), 0777);
				echo "</ul>\n";
				fclose($users_file);
				session_start();
				$_SESSION['username'] = $username;
				header("Location: main.php");
			}
		}
	?>
</body>
</html>

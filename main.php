<!DOCTYPE html>
<html>
<head>
	<title>
		Main Fileshare Page
	</title>
</head>
<body>
	<!-- Upload File Form -->
	<form enctype="multipart/form-data" method="POST">
	<p>
		<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
		<label for="uploadfile_input">Choose a file to upload:</label> <input name="uploadedfile" type="file" id="uploadfile_input" />
	</p>
	<p>
		<input type="submit" value="Upload File" />
	</p>
	</form>

	<!-- View or Delete File Form -->
	<form action="vod.php" method="POST">
	<p>
		Choose a file to view/delete:
		<input type="text" name="filename"/> <br>
		View:
		<input type="radio" name="operation" value="view" checked> <br>
		Delete:
		<input type="radio" name="operation" value="delete"> <br>
	</p>
	<p>
		<input type="submit" value="Submit" />
	</p>
	</form>

	<!-- Logout Button -->
	<form action="logout.php">
		<input type="submit" value="Logout">
	</form>

	<?php
		// Listing User Files
		session_start();
		$username = $_SESSION['username'];
		foreach(array_slice(scandir(sprintf("/srv/uploads/%s", $username)), 2) as $key => $value){
			printf("<p>File %s: %s</p>", $key+1, $value);
		}

		// Uploading Files
		// Get the filename and make sure it is valid
		if(isset($_FILES['uploadedfile'])){
			$filename = basename($_FILES['uploadedfile']['name']);
			if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
				echo "Invalid filename";
				exit;
			}

			// Get the username and make sure it is valid
			if( !preg_match('/^[\w_\-]+$/', $username) ){
				echo "Invalid username";
				exit;
			}

			$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

			if( move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $full_path) ){
				printf("<p>%s</p>", "SUCCESS");
				header("Location: main.php");
			}else{
				printf("<p>%s</p>", "FAILURE");
				exit;
			}
		}
	?>
</body>
</html>


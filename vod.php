<?php
	session_start();
	$username = $_SESSION['username'];
	$filename = $_POST['filename'];
	$operation = $_POST['operation'];
	$file_path = sprintf("/srv/uploads/%s/%s", $username, $filename);

	// Check for file existence
	$validname = false;
	foreach(array_slice(scandir(sprintf("/srv/uploads/%s", $username)),2) as $key => $value){
		if ($filename == $value){
			$validname = true;
		}
	}
	if ($validname == false){
		printf("<p>%s</p>", "FAILURE: FILE DOES NOT EXIST IN USER DIRECTORY!");
		exit;
	}

	// Now that we know file exists, we must either view or delete it
	else{
		if ($operation == "delete"){
			unlink($file_path);
			header("Location: main.php");
		}

		else {
			if (split('\.', $filename)[1] == "pdf"){
				header('Content-type: application/pdf');
				header(sprintf('Content-Disposition: inline; filename="%s"', $filename));
				@readfile($file_path);
			}
			if (split('\.', $filename)[1] == "jpg" or split('\.', $filename)[1] == "png"){
				header('Content-type: image/png');
				header(sprintf('Content-Disposition: inline; filename="%s"', $filename));
				@readfile($file_path);
			}
			if (split('\.', $filename)[1] == "txt"){
				header(sprintf('Content-Disposition: inline; filename="%s"', $filename));
				@readfile($file_path);
			}
		}
	}
?>